use clap::{Parser, Subcommand};
use git2::{Commit, Config, Oid, Repository, RepositoryOpenFlags, Revspec, Signature, Time};
use sequoia_sop::*;
use std::fs::File;
use std::io::Cursor;
use std::io::Write;
use std::os::unix::ffi::OsStringExt;
use std::path::PathBuf;
use std::process::ExitCode;
use tracing::field::DebugValue;
use tracing::{debug, error, info, instrument, trace, warn, Level};
use tracing_subscriber::FmtSubscriber;

//fn parse_signature(s: &str) -> Result<Signature, git2::Error> {
//    let name;
//    let email;
//    let open = s.position("<");
//    Signature::new(name, email, &Time::new(0, 0))
//}

#[instrument(skip_all, fields(commit_id), ret, err)]
fn verify_commit(
    repo: &Repository,
    config: &Config,
    certs: &mut Cursor<Vec<u8>>,
    commit: Commit,
) -> Result<Vec<sop::Verification>, sop::Error> {
    let commit_id = commit.id();
    // TODO tracing::Span::current().record("commit_id", &commit_id.to_string());
    let headers = commit.raw_header().unwrap();
    debug!(%headers);
    // TODO 4. sshsig
    let (signature_armored, signature_data) =
        repo.extract_signature(&commit_id, Some("gpgsig")).unwrap();
    let mut file = File::create("data-tmp.txt").unwrap();
    file.write_all(&signature_data).unwrap();
    let signature_armored = signature_armored.as_str().unwrap();
    let signature_data = signature_data.as_str().unwrap();
    debug!(%signature_armored, %signature_data);

    // TODO 3. take date of commit and signature into account
    // TODO 3. take mail of commiter adress into account
    // TODO 3. take mailmap into account
    let sop = SQOP::default();
    let signature = sop
        .dearmor()
        .unwrap()
        .data(&mut Cursor::new(signature_armored))
        .unwrap()
        .to_vec()
        .unwrap();
    debug!(?signature);

    let cs = &mut Cursor::new(signature);
    let verifyer = sop.verify().unwrap().certs(certs).unwrap();
    let verifications = verifyer.
        // TODO 3. make faster by parsing certs once, the above has not implemented clone().
        // TODO 3. or better make faster by only loading the certs for the current signatures
        signatures(cs).unwrap().
        data(&mut Cursor::new(signature_data));

    let trailers = git2::message_trailers_strs(commit.message_raw().unwrap()).unwrap();
    if Ok(true) == config.get_bool("verify.signOff") {
        // TODO 3. normalise both with mailmap
        // TODO 2. compare to author
        // TODO 2. or commiter?

        // walk this commit and any commits it included by merging
        let mut walk = repo.revwalk().unwrap();
        walk.reset().unwrap();
        walk.set_sorting(git2::Sort::TOPOLOGICAL | git2::Sort::REVERSE);
        walk.push(commit_id);
        // TODO 3. test that this also hides ancestors
        if let Some(first_parent) = commit.parents().next() {
            walk.hide(first_parent.id());
        }
        for Oid in walk {
            let Oid = Oid.unwrap();
            let commit = repo.find_commit(Oid).unwrap();
            debug!("checking signoff on {:?}", Oid.to_string());

            let mut ok = false;
            let author = commit.author();
            for (name, value) in trailers.iter() {
                if name == "Signed-off-by" {
                    //let s = parse_signature(value).unwrap();
                    //if s.name() == author.name() && s.email() == author.email() {
                    info!("signoff ok");
                    ok = true;
                    //}
                }
            }
            if !ok {
                panic!("signoff fail");
            }
        }
    }
    // TODO 2. default to 2
    if let Ok(required) = config.get_i32("verify.reviews") {
        let mut ok = false;
        if required == 0 {
            ok = true;
        } else if required != 1 {
            panic!("reviews is only implemented for 0 or 1")
        }
        for (name, value) in trailers.iter() {
            if name == "Reviewed-by" {
                info!("reviews ok");
                ok = true;
            }
        }
        if !ok {
            panic!("reviews fail");
        }
    }
    // TODO 2. default to true
    if Ok(true) == config.get_bool("verify.reproduceMerges") {
        //TODO 2. implement
        panic!("reproduce merge fail");
    }

    verifications
}

#[instrument(skip_all, ret)]
fn verify_repo(
    repo: &Repository,
    pgp_subdir: &PathBuf,
    config: &Config,
    spec: Revspec,
) -> (
    Vec<Result<Vec<sop::Verification>, sop::Error>>,
    Vec<Result<Vec<sop::Verification>, sop::Error>>,
) {
    // TODO 3. trace some info about repo

    // TODO 2. better defaults
    // TODO 3. support reading from git tree object in bare repo, index in working tree
    // TODO 4. support checking default branch for newer .gittrust content
    let mut pgp_path = PathBuf::from(repo.workdir().unwrap());
    pgp_path.push(pgp_subdir);
    // TODO 2. validate that this is inside git repo
    let mut certs = vec![0; 1024 * 1024];
    debug!(?pgp_path);
    for entry in std::fs::read_dir(pgp_path).unwrap() {
        let entry = entry.unwrap();
        let name = entry.file_name();
        debug!(?name);
        let name = name.into_vec();
        if !name.ends_with(".asc".as_bytes()) {
            debug!("skipped");
            continue;
        }
        let cert = std::fs::read(entry.path()).unwrap();
        debug!("added");
        certs.append(&mut cert.clone());
    }
    // TODO 4. debug dump indentities from certs

    let mut verr: Vec<Result<Vec<sop::Verification>, sop::Error>> = Vec::new();
    let mut vok: Vec<Result<Vec<sop::Verification>, sop::Error>> = Vec::new();

    let mut walk = repo.revwalk().unwrap();
    walk.reset().unwrap();
    walk.set_sorting(git2::Sort::TOPOLOGICAL | git2::Sort::REVERSE);
    walk.simplify_first_parent();
    // TODO from and to are switched in gi2::Revspec?
    let object_from;
    if let Some(o) = spec.to() {
        object_from = o;
    } else {
        object_from = spec.from().unwrap();
    }
    debug!("walk include {:?}", object_from.id().to_string());
    walk.push(object_from.id());
    if spec.mode() == git2::RevparseMode::SINGLE {
        debug!("walking everything reachable from single revision");
    } else if spec.mode().is_range() {
        if let Some(object_to) = spec.from() {
            debug!("walk exclude first part of range {:?}", object_to.id().to_string());
            walk.hide(object_to.id());
        }
    } else if spec.mode().is_merge_base() {
        panic!("Do not use three dots in the refspec, only two dots are supported.");
    } else {
        unreachable!("Unexpectated state of the mode of the refspec!")
    }

    let certs = &mut Cursor::new(certs);
    for Oid in walk {
        certs.set_position(0);
        let commit = repo.find_commit(Oid.unwrap()).unwrap();
        let verifications = verify_commit(repo, config, certs, commit);
        if verifications.is_ok() {
            vok.push(verifications);
        } else {
            verr.push(verifications);
        }
    }
    (verr, vok)
}

#[instrument]
fn cert_import(pgp_path: &PathBuf, cert_file: &PathBuf) {
    // TODO 2. implement with fail on exist
    // TODO 3. merge
    let cert = std::fs::read(cert_file).unwrap().into_boxed_slice();
}

// TODO 2. cleanup
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Use path as a Git repo
    #[clap(short, long, value_name = "git")]
    git: Option<PathBuf>,

    /// Use subdir in Git repo for OpenPGP certificates
    #[clap(short, long, value_name = "subdir")]
    pgp: Option<PathBuf>,

    #[clap(subcommand)]
    command: Option<Commands>,

    #[clap(default_value = "HEAD~1..HEAD")]
    refspec: String,
}

#[derive(Subcommand, Debug)]
enum Commands {
    /// oprations on the cert store
    Cert {
        /// Import cert in asc file
        #[clap(value_name = "asc")]
        import: PathBuf,
    },
}

#[instrument]
fn config(path: Option<PathBuf>) -> (Repository, Config) {
    let repo;
    if let Some(path) = path {
        repo = Repository::open(path).unwrap();
    } else {
        repo = Repository::open_from_env().unwrap();
        //TODO 2. the trait `AsRef<OsStr>` is not implemented for `(OsString, OsString)`
        //let repo = Repository::open_ext("../podman", RepositoryOpenFlags::FROM_ENV, std::env::vars_os()).unwrap();
    }

    let mut config_path = PathBuf::from(repo.workdir().unwrap());
    // TODO 2. from cli args
    // TODO 2. add aditional layers .git/config , etc
    // TODO 2. unify with cli arguments?
    config_path.push(".gittrust/config");

    let config = Config::open(&config_path).unwrap();
    (repo, config)
}

#[instrument(ret)]
fn main() -> ExitCode {
    // TODO 3. write test fixture generator, then commit fixtures as archive in git
    // TODO 3. replace all unwrap with proper error messages or handling
    // TODO 3. --verbose and alias --debug and lower default verbose level
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::DEBUG)
        .finish();
    tracing::subscriber::set_global_default(subscriber).unwrap();

    warn!("WARNING: proof of concept, only use this software for testing!");
    warn!("This had no security review yet, do not rely on it as a security mechanism.");

    let args = Args::parse();

    match &args.command {
        Some(Commands::Cert { import }) => {
            let mut pgp_path = PathBuf::from(args.git.unwrap());
            pgp_path.push(args.pgp.unwrap());
            cert_import(&pgp_path, import)
        }
        None => {
            let (repo, config) = config(args.git);
            let pgp_path;
            if let Some(path) = args.pgp {
                pgp_path = path;
            } else if let Ok(s) = config.get_string("verify.pgpCertD") {
                pgp_path = PathBuf::from(s);
            } else {
                pgp_path = PathBuf::from(".gittrust/pgp-cert.asc.d");
            }
            let spec = repo.revparse(&args.refspec).unwrap();
            let (verr, vok) = verify_repo(&repo, &pgp_path, &config, spec);
            if verr.is_empty() {
                println!("Good!");
                return ExitCode::SUCCESS;
            }
        }
    }
    println!("NO!");
    return ExitCode::FAILURE;
}
